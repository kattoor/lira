const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/songs.json', (req, res) => {
    const songs = JSON.parse(fs.readFileSync('./data/songs.json'));
    res.send(JSON.stringify(songs.slice(0, 5)));
});

app.post('/add-song', (req, res) => {
    const songs = JSON.parse(fs.readFileSync('./data/songs.json'));
    if (req.body.song && req.body.song.length > 0)
        songs.unshift(req.body.song);
    fs.writeFileSync('./songs.json', JSON.stringify(songs));
    res.send(JSON.stringify(songs.slice(0, 5)));
});

app.get('/comments.json', (req, res) => {
    const comments = JSON.parse(fs.readFileSync('./data/comments.json'));
    res.send(JSON.stringify(comments.slice(0, 5)));
});

app.post('/add-comment', (req, res) => {
    const comments = JSON.parse(fs.readFileSync('./data/comments.json'));
    if (req.body.comment && req.body.comment.length > 0)
        comments.unshift(req.body.comment);
    fs.writeFileSync('./comments.json', JSON.stringify(comments));
    res.send(JSON.stringify(comments.slice(0, 5)));
});

app.get('/picture-info', (req, res) => {
    const thumbnails = fs.readFileSync('./thumbnails');
    res.send(thumbnails);
});

app.use(express.static('.'));
app.listen(80);
