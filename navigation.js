[].forEach.call(document.getElementsByClassName('navButton'), function (element) {
    element.addEventListener('click', function (e) {
        console.log(e.target.innerHTML);
        navigateTo(e.target.innerHTML.toLowerCase())
    });
});

function navigateTo(page) {
    page = page.replace('<span>', '').replace('</span>', '');
    window.location.href = prefix + (page === 'start' ? 'index.html' : (page.replace('<span>', '').replace('</span>', '') + '.html'));
}

function setHighlight(page) {
    document.getElementById(page).style['background-color'] = '#D35400';
}
